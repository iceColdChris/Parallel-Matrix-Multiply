#ifdef __APPLE__
#include <sys/time.h>
#define STOPWATCH_TYPE struct timeval
#define STOPWATCH_CLICK(x) gettimeofday(&x, NULL)
#else
#define _POSIX_C_SOURCE 199309
#include <time.h>
#define STOPWATCH_TYPE struct timespec
#define STOPWATCH_CLICK(x) clock_gettime(CLOCK_REALTIME, &x)
#endif

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_THREADS 4

long ms_diff(STOPWATCH_TYPE start, STOPWATCH_TYPE stop);

typedef struct ThreadHelper{
    const int *A;
    const int *B;
    int *C;
    int StartA;
    int EndA;
    int r;
    int s;
    int t;
    
}thread_helper;

void* multiply(void* arg)
{
    int i, j, k;
    
    thread_helper *helper = (thread_helper*)arg;
    
    for(i =helper->StartA; i < helper->EndA ; i ++)
    {
        for(j=0; j < helper->t; j ++)
        {
            int sum = 0;
            for(k = 0; k < helper->s; k++)
            {
                sum = sum + helper->A[i* helper->s+k] * helper->B[k * helper->t+j];
            }
            helper->C[i * helper->t + j] = sum;
        }
    }
    
    return NULL;
}

int * multithreaded_matrix_product(const int A[], const int B[], int r, int s, int t) {
    
    int *C = malloc(r * t * sizeof(int));
    
    /*
     * Creates a thread helper that multiplies
     * over the first fourth of the matrix A
     */
    thread_helper Th1;
    Th1.A = A;
    Th1.B = B;
    Th1.C = C;
    Th1.StartA = 0;
    Th1.EndA = r/4;
    Th1.r = r;
    Th1.s = s;
    Th1.t = t;
    
    /*
     * Creates a thread helper that multiplies
     * over the second fourth of the matrix A
     */
    thread_helper Th2;
    Th2.A = A;
    Th2.B = B;
    Th2.C = C;
    Th2.StartA = r/4;
    Th2.EndA = r/2;
    Th2.r = r;
    Th2.s = s;
    Th2.t = t;
    
    /*
     * Creates a thread helper that multiplies
     * over the third fourth of the matrix A
     */
    thread_helper Th3;
    Th3.A = A;
    Th3.B = B;
    Th3.C = C;
    Th3.StartA = r/2;
    Th3.EndA = r - (r/4);
    Th3.r = r;
    Th3.s = s;
    Th3.t = t;
    
    /*
     * Creates a thread helper that multiplies
     * over the remaining rows of the matrix A
     */
    thread_helper Th4;
    Th4.A = A;
    Th4.B = B;
    Th4.C = C;
    Th4.StartA = r - (r/4);
    Th4.EndA = r;
    Th4.r = r;
    Th4.s = s;
    Th4.t = t;
    
    thread_helper storage[] = {Th1, Th2, Th3, Th4};
    
    // Create and start each thread.
    int i;
    pthread_t threads[MAX_THREADS];
    for (i = 0; i < MAX_THREADS; i++)
        if (pthread_create(&threads[i], NULL, multiply, &storage[i]) != 0) {
            printf("ERROR: Unable to create new thread.\n");
            exit(EXIT_FAILURE);
        }
    
    void * tmp;
    for (i = 0; i < MAX_THREADS; i++) {
        pthread_join(threads[i], &tmp);
        
    }
    
    return C;
}

// Creates an r-by-s matrix with random elements in the range [-5, 5].
int * create_random_matrix(int r, int s) {
    int size = r * s;
    int * matrix = (int *)malloc(size * sizeof(int));
    int i;
    for (i = 0; i < size; i++)
        matrix[i] = rand() % 11 - 5;
    return matrix;
}
/*
 * Test's the matrix multiplication algorithm
 * for correctness using smaller numbers.This
 * code was moved to account for performance
 * issues due to the conflict between this code and
 * the test speed function.
 *
 * Prints out
 * -----------
 * Matrix A
 * Matrix B
 * Matrix A*B
 */
void testMatrixMultiply()
{
    int i;
    
    const int r = 4;
    const int s = 4;
    const int t = 4;
    int  *A = create_random_matrix(r, s);
    int  *B = create_random_matrix(s, t);
    
    for(i = 0; i < r*s; i++)
    {
        printf("%d ", A[i]);
    }
    
    printf("\n");
    
    for(i = 0; i < s*t; i++)
    {
        printf("%d ", B[i]);
    }
    
    printf("\n");
    
    
    int * C = multithreaded_matrix_product(A, B, r, s, t);
    
    for(i = 0; i < r*t; i++)
    {
        printf("%d ", C[i]);
    }
    
    free(A);
    free(B);
    free(C);
}

/*
 * The default code from main moved into a
 * new method. This code was unedited just
 * moved to account for performance issues
 * due to the conflict between this code and
 * the test matrix multiply.
 */
void testSpeed()
{
    const int r = 1000;
    const int s = 2000;
    const int t = 1000;
    int  *A = create_random_matrix(r, s);
    int  *B = create_random_matrix(s, t);
    
    STOPWATCH_TYPE start;
    STOPWATCH_CLICK(start);
    
    int * C = multithreaded_matrix_product(A, B, r, s, t);
    
    STOPWATCH_TYPE stop;
    STOPWATCH_CLICK(stop);
    
    
    printf("\n %ld ms elapsed.\n", ms_diff(start, stop));
    free(A);
    free(B);
    free(C);
    
}

int main(int argc, char* argv[]) {
    srand(time(NULL));
    
    
    /* TODO Change These
     * Only have one of these running at a time
     * If you have both the test matrix multiply and test
     * speed it affects performance.
     */
    
    //testMatrixMultiply();
    testSpeed();
    
    return EXIT_SUCCESS;
}

#ifdef __APPLE__
long ms_diff(struct timeval start, struct timeval stop) {
    return 1000L * (stop.tv_sec - start.tv_sec) + (stop.tv_usec - start.tv_usec) / 1000;
}
#else
long ms_diff(struct timespec start, struct timespec stop) {
    return 1000L * (stop.tv_sec - start.tv_sec) + (stop.tv_nsec - start.tv_nsec) / 1000000;
}
#endif
